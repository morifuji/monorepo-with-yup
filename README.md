# monorepo-with-yup

Typescriptで、バリデーションライブラリのyupを使った、フロントエンド・バックエンドで共有するAPIの型・バリデーションを持つことができるかどうか検証

## 構成

個々のパッケージをyarn workspaceで管理

- frontend
    - react
    - `yarn start`でdevサーバー起動
- backend
    - express
    - `yarn buuld && yarn start`でdevサーバー起動
- common
    - バリデーション・型を配置
    - `yarn build`でビルド

### common

エンドポイントごとにリクエストとレスポンスの型およびパスを定義

```typescript
//////////
// ログイン
//////////
export const ReqBodyLogin = yup.object({
    ...EmailAndPassword
});
export type TypeReqBodyLogin = yup.InferType<typeof ReqBodyLogin>
export const ResBodyLogin = yup.object({
    accessToken: yup.string()
})
export type TypeResBodyLogin = yup.InferType<typeof ResBodyLogin>
export const EndpointLogin: Endpoint = {
    path: "/login",
    request: ReqBodyLogin,
    response: ResBodyLogin
}
```


### backend

importして利用

```typescript
app.post(Common.User.EndpointLogin.path, async (req: Request<Common.User.TypeReqBodyLogin>, res: Response<Common.User.TypeReqBodySignUp>) => {
  try {
    await Common.validate(Common.User.ReqBodyLogin, req.body)
    // brabra....
  } catch (err) {
    return res.status(400).json(err);
  }
})
```

### frontend

importして利用

```typescript
import * as Common from 'monorepo-with-yup-common';

// バックエンドバリデーション
await axiosInstance.post(Common.User.EndpointLogin.path, body).then(()=>{
    setServerErrorData(null)
})
```

### 結果

- リクエストボディの型・バリデーションは問題なく共通化できた
    - これによって、フロントエンド・バックエンド双方で完全に一致したバリデーションを行うことができる。以下画像の通り、フロントエンド・バックエンドでエラーメッセージが同一になる
    - 付随してリクエストクエリパラメータの型・エンドポイントのパスもまとめて共通化できた
    - 利用する側で`InferType` などこねこねする必要があるのでこのままでは実用的ではない
- 同様の手法でレスポンスボディのバリデーションも含めることもできる見込み

![](./demo.png)

