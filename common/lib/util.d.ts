import * as yup from "yup";
export declare type Endpoint = {
    path: string;
    request: yup.AnySchema;
    response: yup.AnySchema;
};
//# sourceMappingURL=util.d.ts.map