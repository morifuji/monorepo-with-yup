import * as yup from "yup";
export declare type Endpoint = {
    path: string;
    request: yup.AnySchema;
    response: yup.AnySchema;
};
export declare const ReqBodyLogin: import("yup/lib/object").OptionalObjectSchema<{
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}, import("yup/lib/object").AnyObject, import("yup/lib/object").TypeOfShape<{
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}>>;
export declare type TypeReqBodyLogin = yup.InferType<typeof ReqBodyLogin>;
export declare const ResBodyLogin: import("yup/lib/object").OptionalObjectSchema<{
    accessToken: yup.StringSchema<string | undefined, import("yup/lib/types").AnyObject, string | undefined>;
}, import("yup/lib/object").AnyObject, import("yup/lib/object").TypeOfShape<{
    accessToken: yup.StringSchema<string | undefined, import("yup/lib/types").AnyObject, string | undefined>;
}>>;
export declare type TypeResBodyLogin = yup.InferType<typeof ResBodyLogin>;
export declare const EndpointLogin: Endpoint;
export declare const ReqBodySignUp: import("yup/lib/object").OptionalObjectSchema<{
    dateOfBirth: import("yup/lib/date").RequiredDateSchema<Date | undefined, import("yup/lib/types").AnyObject>;
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}, import("yup/lib/object").AnyObject, import("yup/lib/object").TypeOfShape<{
    dateOfBirth: import("yup/lib/date").RequiredDateSchema<Date | undefined, import("yup/lib/types").AnyObject>;
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}>>;
export declare type TypeReqBodySignUp = yup.InferType<typeof ReqBodySignUp>;
export declare const ResBodySignUp: import("yup/lib/object").OptionalObjectSchema<{
    dateOfBirth: import("yup/lib/date").RequiredDateSchema<Date | undefined, import("yup/lib/types").AnyObject>;
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}, import("yup/lib/object").AnyObject, import("yup/lib/object").TypeOfShape<{
    dateOfBirth: import("yup/lib/date").RequiredDateSchema<Date | undefined, import("yup/lib/types").AnyObject>;
    email: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
    password: import("yup/lib/string").RequiredStringSchema<string | undefined, import("yup/lib/types").AnyObject>;
}>>;
export declare type TypeResBodySignUp = yup.InferType<typeof ResBodySignUp>;
export declare const EndpointSignUp: Endpoint;
//# sourceMappingURL=User.d.ts.map