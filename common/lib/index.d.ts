import * as yup from "yup";
export declare const validate: <T extends yup.AnySchema<any, any, any>>(schema: T, data: any) => Promise<T["__outputType"]>;
export * as User from "./User";
//# sourceMappingURL=index.d.ts.map