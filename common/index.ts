import * as yup from "yup";
export const validate = <T extends yup.AnySchema>(schema: T, data: any) => schema.validate(data, { abortEarly: false })
// export type InferType<T extends yup.AnySchema> = yup.InferType<T>

export * as User from "./User"

