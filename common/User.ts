import * as yup from "yup";

const EmailAndPassword = {
    email: yup.string().required().email(),
    password: yup.string().required().max(20)
}

export type Endpoint = {
    path: string
    request: yup.AnySchema
    response: yup.AnySchema
}

//////////
// ログイン
//////////
export const ReqBodyLogin = yup.object({
    ...EmailAndPassword
});
export type TypeReqBodyLogin = yup.InferType<typeof ReqBodyLogin>
export const ResBodyLogin = yup.object({
    accessToken: yup.string()
})
export type TypeResBodyLogin = yup.InferType<typeof ResBodyLogin>
export const EndpointLogin: Endpoint = {
    path: "/login",
    request: ReqBodyLogin,
    response: ResBodyLogin
}


//////////
// 会員登録
//////////
export const ReqBodySignUp = yup.object({
    ...EmailAndPassword,
    dateOfBirth: yup.date().required().max(new Date())
});
export type TypeReqBodySignUp = yup.InferType<typeof ReqBodySignUp>
export const ResBodySignUp = yup.object({
    ...EmailAndPassword,
    dateOfBirth: yup.date().required().max(new Date())
});
export type TypeResBodySignUp = yup.InferType<typeof ResBodySignUp>

export const EndpointSignUp: Endpoint = {
    path: "/signup",
    request: ReqBodySignUp,
    response: ResBodySignUp
}

