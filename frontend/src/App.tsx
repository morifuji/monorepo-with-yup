import React, { useCallback, useState } from 'react';
import './App.css';
import axios from "axios"
import * as Schema from 'monorepo-with-yup-common';
import { format, parse } from 'date-fns';

const axiosInstance = axios.create({
  baseURL: "http://localhost:8080/"
})
function App() {


  const [email, setEmail] = useState("")
  const [password, setpassword] = useState("")
  const [dateOfBirth, setDateOfBirth] = useState(new Date())

  const [clientErrorData, setClientErrorData] = useState<null | any>(null)
  const [serverErrorData, setServerErrorData] = useState<null | any>(null)


  const login = useCallback(async () => {
    const body = {
      email,
      password
    }

    await Promise.allSettled(
      [
        Schema.validate(Schema.User.ReqBodyLogin, body)
        .then(() => {
          setClientErrorData(null)
        })
        .catch(err => {
          setClientErrorData(err)
          throw err
        }),
        axiosInstance.post<Schema.User.TypeResBodyLogin>(Schema.User.EndpointLogin.path, body).then((res) => {
          console.log(res.data.accessToken) // 型が補完される
          setServerErrorData(null)
          return res
        })
        .catch(err => {
          setServerErrorData(err.response.data)
          throw err
        })
      ]
    )
  }, [email, password])

  return (
    <div className="App">
      <h1>ログイン</h1>
      <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
      <input type="password" value={password} onChange={e => setpassword(e.target.value)} />
      <button onClick={login}>ログイン</button>
      <hr />

      <h1>会員登録</h1>
      <input type="text" value={email} onChange={e => setEmail(e.target.value)} />
      <input type="password" value={password} onChange={e => setpassword(e.target.value)} />
      <input type="date" value={format(dateOfBirth, "yyyy-MM-dd")} onChange={e => setDateOfBirth(parse(e.target.value, "yyyy-MM-dd", new Date(1900, 0, 1, 0, 0, 0)))} />
      <button onClick={login}>会員登録</button>

      <hr />
      クライアントバリデーション結果↓
      <br />
      {JSON.stringify(clientErrorData) ?? null}
      <br />
      サーバーバリデーション結果↓
      <br />
      {JSON.stringify(serverErrorData) ?? null}
    </div>
  );
}

export default App;
