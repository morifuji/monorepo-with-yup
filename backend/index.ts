import express, { Request, Response } from 'express'
import * as Schema from "monorepo-with-yup-common"
import cors from "cors"
import bodyParser from 'body-parser';

const app = express();
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('hello from express🍣');
});

app.listen(8080, () => {
  console.log(`[server]: Server is running at https://localhost:8080`);
});

app.post(Schema.User.EndpointLogin.path, async (req: Request<Schema.User.TypeReqBodyLogin>, res: Response<Schema.User.TypeReqBodySignUp>) => {
  console.log(req.body)
  try {
    await Schema.validate(Schema.User.ReqBodyLogin, req.body)
    console.info("success!")
  } catch (err) {
    return res.status(400).json(err as any);
  }
})

app.post(Schema.User.EndpointSignUp.path, async (req: Request<Schema.User.TypeReqBodySignUp>, res) => {
  try {
    await Schema.validate(Schema.User.ReqBodySignUp, req.body)
    console.info("success!")
  } catch (err) {
    return res.status(400).json(err as any);
  }
})
